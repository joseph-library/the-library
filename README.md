# The Library

## Overview

“图书馆”，顾名思义，这里是藏书的地方。更准确一点说，这个仓库存放了D.T. Joseph的作品。

## License

本仓库内所有内容以[知识共享 署名-非商业性使用-禁止演绎 4.0 国际（CC BY-NC-ND 4.0）协议](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.zh)发布。若要转载请给出适当的署名，并给出指向本仓库的链接，且不可以用于商业目的。
